﻿using System.Text.RegularExpressions;
using System;

namespace AttemptAnConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            bool game = true;
            Console.WriteLine("Welcome to the game, anytime you want to quit, type 'q'");
            while (game)
            { 
                int answer = r.Next(1, 9);
                bool cont = true;

                while (cont)                 
                {
                    Console.WriteLine("Guess a number between 1 and 9");
                    char userInput = Console.ReadKey().KeyChar;
                    if (!IsValidInput(userInput))
                    {
                        continue;
                    }
                    if(userInput == 'q')
                    {
                        game = false;
                        break;
                    }
                    
                    int IntUserInput = int.Parse(userInput.ToString());


                    if (IntUserInput == answer) 
                    {
                        string message = Environment.NewLine + "That is correct!";
                        Console.WriteLine(message);
                        cont = false;
                    }
                    else
                    {
                        string message = Environment.NewLine + "That is not the number I thought of!";
                        Console.WriteLine(message);
                        Console.WriteLine("Try again!");
                    }
                }
                if (game == false)
                {
                    Console.WriteLine();
                    break;
                }
                Console.WriteLine(Environment.NewLine + "Let's play more! Remember you can type q to exit!");
                Console.WriteLine();
                
            }
            Console.WriteLine("Thanks for playing, exiting...");
        }
        
        static bool IsValidInput(char character)
        {
            Regex ValidInput = new Regex("([1-9]|q)");
            Match Inputmatch = ValidInput.Match(character.ToString());
            if (Inputmatch.Success)
            {
                return true;
            }
            Console.WriteLine();
            Console.WriteLine("that is not a valid input, input a number between 1 and 9, or 'q' to quit");
            return false;
        }
    }
}
